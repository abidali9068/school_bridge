
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/ExampleComponent.vue'));
Vue.component('States',require('./components/States.vue'));
Vue.component('Areas',require('./components/Areas.vue'));
Vue.component('Religions',require('./components/Religions.vue'));
Vue.component('Bloodgroups',require('./components/BloodGroups.vue'));
Vue.component('Maritalstatus',require('./components/MaritalStatus.vue'));
Vue.component('Cities',require('./components/Cities.vue'));
Vue.component('Schools',require('./components/Schools.vue'));
Vue.component('Departments',require('./components/Departments.vue'));
Vue.component('Designations',require('./components/Designations.vue'));
Vue.component('Jobtypes',require('./components/JobTypes.vue'));

const app = new Vue({
    el: '#app'
});
