  
<!DOCTYPE html>
<html>
<head>
	<title>State</title>
	 <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
	<div class="container">
		<div id="app">
      <Designations></Designations>
  </div>

  <script src="{{ asset('js/app.js') }}"></script>
	</div>
</body>
</html>
  