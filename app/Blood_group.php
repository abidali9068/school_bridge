<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blood_group extends Model
{
     protected $fillable = ['blood_group_name','description','isArchive','created_by'];
}
