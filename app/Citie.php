<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citie extends Model
{
     protected $fillable = ['city_name','city_code','description','isArchive','created_by'];
}
