<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State as state;
use App\Area as area;
use App\Religion as religion;
use App\Blood_group as bloodgroup;
use Auth;

class AdminController extends Controller
{

public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	return view('home');
    }
//state
    public function viewStates()
    {
        return view('settings/state');
    }

    public function getStates(){
        return State::where('isArchive',0)->orderBy('id','desc')->get();
    }

    public function addState(Request $req){
        $state_name = $req->input('state_name');
      
        $description = $req->input('description');
        $state = state::create(['state_name'=>$state_name,'description'=>$description,'isArchive'=>0, 'created_by'=>Auth::user()->id]);
        return $state;
    }
//area
    public function viewAreas()
    {
        return view('settings/area');
    }

    public function getArea(){
        return area::where('isArchive',0)->orderBy('id','desc')->get();
    }

    public function addAreas(Request $req){
        $area_name = $req->input('area_name');
      
        $description = $req->input('description');
        $area = area::create(['area_name'=>$area_name,'description'=>$description,'isArchive'=>0, 'created_by'=>Auth::user()->id]);
        return $area;
    }

//religion
    public function viewReligions()
    {
        return view('settings/religion');
    }

    public function getReligions(){
        return religion::where('isArchive',0)->orderBy('id','desc')->get();
    }

    public function addReligion(Request $req){
        $religion_name = $req->input('religion_name');
      
        $description = $req->input('description');
        $religion = religion::create(['religion_name'=>$religion_name,'description'=>$description,'isArchive'=>0, 'created_by'=>Auth::user()->id]);
        return $religion;
    }


//bloodgroup
    public function viewBloodGroups()
    {
        return view('settings/blood_group');
    }

    public function getBloodGroups(){
        return bloodgroup::where('isArchive',0)->orderBy('id','desc')->get();
    }

    public function addBloodGroups(Request $req){
        $blood_group_name = $req->input('blood_group_name');
      
        $description = $req->input('description');
        $bloodgroup = bloodgroup::create(['blood_group_name'=>$blood_group_name,'description'=>$description,'isArchive'=>0, 'created_by'=>Auth::user()->id]);
        return $bloodgroup;
    }





}
