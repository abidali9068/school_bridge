<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School as school;
use Auth;

class SchoolController extends Controller
{
    public function viewSchools()
    {
        return view('settings/school');
    }

    public function getSchools(){
        return school::where('isArchive',0)->orderBy('id','desc')->get();
    }

    public function addSchool(Request $req){
        $city_name = $req->input('school_name');
        $address = $req->input('address');
         $campus = $req->input('campus');

        $description = $req->input('description');
        $school = school::create(['school_name'=>$city_name,'address'=>$address,'campus'=>$campus,'description'=>$description,'isArchive'=>0, 'created_by'=>Auth::user()->id]);
        return $school;
    }
}
