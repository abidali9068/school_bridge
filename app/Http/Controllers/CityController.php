<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Citie as city;
use Auth;

class CityController extends Controller
{
    //maritalstatus
    public function viewCities()
    {
        return view('settings/city');
    }

    public function getCities(){
        return city::where('isArchive',0)->orderBy('id','desc')->get();
    }

    public function addCity(Request $req){
        $city_name = $req->input('city_name');
        $city_code = $req->input('city_code');
        $description = $req->input('description');
        $city = city::create(['city_name'=>$city_name,'city_code'=>$city_code,'description'=>$description,'isArchive'=>0, 'created_by'=>Auth::user()->id]);
        return $city;
    }
}
