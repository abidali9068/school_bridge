<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Marital_status as ms;
use Auth;

class MaritalStatusController extends Controller
{
   //maritalstatus
    public function viewMaritalStatus()
    {
        return view('settings/marital_status');
    }

    public function getMaritalStatus(){
        return ms::where('isArchive',0)->orderBy('id','desc')->get();
    }

    public function addMaritalStatus(Request $req){
        $marital_status_name = $req->input('marital_status_name');
      
        $description = $req->input('description');
        $maritalstatus = ms::create(['marital_status_name'=>$marital_status_name,'description'=>$description,'isArchive'=>0, 'created_by'=>Auth::user()->id]);
        return $maritalstatus;
    }
}
