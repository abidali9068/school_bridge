<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job_type extends Model
{
    
     protected $fillable = ['job_name','description','isArchive','created_by'];
}
