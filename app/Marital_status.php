<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marital_status extends Model
{
     protected $table = "marital_status";
     protected $fillable = ['marital_status_name','description','isArchive','created_by'];
}
