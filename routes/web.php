<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
Route::prefix('teacher')->group(function(){
	Route::get('states','TeacherController@showStates');
	//Route::get
});
*/

Route::prefix('admin')->group(function(){
	Route::get('states','AdminController@viewStates');
	Route::get('areas','AdminController@viewAreas');
	Route::get('religions','AdminController@viewReligions');
	Route::get('bloodgroup','AdminController@viewBloodGroups');
	Route::get('maritalstatus','MaritalStatusController@viewMaritalStatus');
	Route::get('cities','CityController@viewCities');
	Route::get('schools','SchoolController@viewSchools');
	Route::resource('departments','DepartmentController');
	Route::resource('designations','DesignationController');
	Route::resource('jobtypes','JobTypeController');

});

//state
Route::get('/getStateRecord','AdminController@getStates');

Route::post('/addStateRecord', 'AdminController@addStates');
//area
Route::get('/getAreaRecord','AdminController@getArea');

Route::post('/addAreaRecord', 'AdminController@addAreas');
//religion
Route::get('/getReligionRecord','AdminController@getReligions');

Route::post('/addReligionRecord', 'AdminController@addReligion');

//bloodgroup
Route::get('/getBloodGroupRecord','AdminController@getBloodGroups');

Route::post('/addBloodGroupRecord', 'AdminController@addBloodGroups');


//maritalstatus
Route::get('/getMaritalStatusRecord','MaritalStatusController@getMaritalStatus');

Route::post('/addMaritalStatusRecord', 'MaritalStatusController@addMaritalStatus');


//city
Route::get('/getCityRecord','CityController@getCities');

Route::post('/addCityRecord', 'CityController@addCity');

//school
Route::get('/getSchoolRecord','SchoolController@getSchools');

Route::post('/addSchoolRecord', 'SchoolController@addSchool');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

